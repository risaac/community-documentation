LibreHealth EHR
---------------

About the project
=================

The LibreHealth EHR application is a clinically-focused electronic health record (EHR) system designed to be both easy to use “out of the box” and also customizable for use in a variety of health care settings. It builds on the strength of the LibreHealth Toolkit, and adapts many of the proven user experiences built over many years with OpenEMR. It is designed to be a modern, easy-to-use experience for health care professionals in their daily work.


Maintainer
==========

Tony McCormick

Demo
====

Demo information can be found `here <https://librehealth.io/demos>`_.

Code Repository
===============

`<https://github.com/LibreHealthIO/lh-ehr>`_.

Discussion Forum
================

`<https://forums.librehealth.io/c/projects/lh-ehr>`_
