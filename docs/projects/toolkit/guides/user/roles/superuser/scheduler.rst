
System Developer: Scheduler
===========================

Introduction
------------

System Developer can manage scheduled tasks.

Procedure
---------

Logging in as a System Developer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need to be logged in as a System Developer to proceed with this tutorial. Navigate to ``Administration`` tab on the main dashboard on the top of the page and locate ``Scheduler`` chapter.


.. image:: images/scheduler.png
   :alt: scheduler


Available operations
^^^^^^^^^^^^^^^^^^^^

Manage Scheduler
~~~~~~~~~~~~~~~~

``Manage Scheduler`` will redirect you to the form where you can either add a new task or edit an existing one.


.. image:: images/scheduler-add-task.png
   :alt: adding a task


Click ``Add Task`` to access the form where you can fill in information about a new task.


.. image:: images/describe-new-task.png
   :alt: describing a new task


Once you are ready, click ``Save``.

You can also edit information about an existing task. Click on the link to the task you want to edit and you will see the same form as when creating a task. You can make your changes here and save them.

Summary
-------

A System Developer can manage scheduled tasks in the system.
