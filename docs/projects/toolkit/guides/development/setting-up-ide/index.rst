Setting up your Integerated Development Environment (IDE)
---------------------------------------------------------

.. toctree::
   :caption: Setting up your Integerated Development Environment (IDE)
   :maxdepth: 1
   :hidden:

   installing-ide
   using-git
   using-gitlab


The LibreHealth toolkit (LH Toolkit) is one of the projects under the LibreHealth group on GitLab. It contains some of the most important software and interfaces required to run efficiently and to coordinate with other projects of LibreHealth like LibreHealth-radiology and open-decision-framework. The table below can be referred to understand the repositories present in the LH Toolkit.

.. list-table::

   * - api/
     - java and resource files for building the java api jar file.
   * - puppet
     - Puppet scripts for local development and deployment.
   * - release-test
     - Cucumber/selenium integration tests. Run daily against a running web app.
   * - test
     - Maven project to share tools that are used for testing.
   * - tools
     - Meta code used during compiling and testing. Does not go into any released binary (like doclets).
   * - web/
     - java and resource files that are used in the web application.
   * - webapp/
     - jsp files used in building the war file.
   * - .gitlab-ci.yaml
     - Used to configure Gitlab CI. Each branch can have its own configuration.
   * - license-header.txt
     - Used by license-maven-plugin to check the license on all source code headers.
   * - pom.xml
     - The main maven file used to build and package OpenMRS.


To have all models processed correctly, this project should be opened under Maven projects. Once the project is forked into the user's profile successfully, it is cloned to the local system easily using the git clone command. Many IDE's are available on the web but IntelliJ IDEA is preferred in this case considering its flexibility in handling complex projects like this.
