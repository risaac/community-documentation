
LibreHealth Toolkit installation
================================

There are different installation options for the LibreHealth Toolkit (LH Toolkit) project.

As this guide is intended primarily for developers, "Advanced" installation is described. Other options can be chosen when needed.


After completing `"Running the project" <../../development/index.html#running-the-project>`_ guide, navigate to the `URL <localhost:8080/openmrs>`_ where the project is running.
Select the desired language and proceed to the next step.


.. image:: images/setup-start.png
   :alt: setup start


The "Advanced" installation gives more flexibility over selection of database url, name and other parameters.

**Step 1:**


.. note:: You need a MySQL database running to install LH Toolkit locally. The easiest way is to run it in a Docker container (see `the guide on Docker installation <../../development/index.html#installation-and-running-with-docker>`_ ). Type the following command in the terminal: ``docker-compose -f docker-compose.dev.yml up -d db``. Hereinafter installation is described as if you are running MySQL database as stated. Select "Advanced" option. It is mainly used in production environments.

.. image:: images/choosing-installation-type.png
   :alt: choosing installation type


On the next screen, enter database name ``toolkit``.  Click ``Next``.


.. image:: images/entering-db-name.png
   :alt: entering db name


**Step 2:**

Answer "No" to the questions on the next screen and provide the following credentials for the database:


* name: ``toolkit``
* password: ``password``


.. image:: images/entering-db-credentials.png
   :alt: entering db credentials


**Step 3:**

Answer "Yes" both times when you are prompted whether to upload the modules from the web interface, and whether to enable automatic updates. Proceed to the next step.


.. image:: images/installation-options.png
   :alt: installation options


**Step 4:**

Step 4 is optional and is skipped in this type of installation. You will get to step 5 after step 3.

**Step 5:**

The next set of settings are optional. You don't need to choose anything here. Click "Next" to review and apply chosen options.


.. image:: images/optional-settings.png
   :alt: optional settings


After completing all the 5 steps of advanced installation, a screen appears, where all the details entered by the user and the installation directory are be specified.


.. image:: images/review-of-chosen-settings.png
   :alt: review of chosen settings


Make necessary changes or click "Next" to finish installation.
You will see progress of completed tasks.


.. image:: images/installation-progress.png
   :alt: installation progress

When installation is finished you will see this information alongside with helpful links.


.. image:: images/installation-success.png
   :alt: installation success
