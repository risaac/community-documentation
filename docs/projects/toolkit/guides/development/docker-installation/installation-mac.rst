Installing Docker on Mac
========================

.. toctree::
   :caption: Installing Docker on Mac


Pre-requisites
~~~~~~~~~~~~~~


#. Docker requires OS X El Capitan 10.11 or newer macOS release running on a 2010 or newer Mac.
#. At least 4GB of RAM in the system.

Installation process
~~~~~~~~~~~~~~~~~~~~


#.
   Download Docker for mac from the url: https://download.docker.com/mac/stable/Docker.dmg

#.
   Click on the Docker.dmg file you have just downloaded to mount the image and open the installer.

   .. image:: images/macOS/step1.png
      :alt: mount the image


#.
   Drag and drop the Docker.app file into the Applications directory. Once you have dragged the Docker icon to your Applications folder, double-click on it and you will be asked whether you want to open the application you have downloaded. Saying yes will open the Docker installer:

   .. image:: images/macOS/step3.png
      :alt: open the Docker installer


#.
   Click next on the installer screen and follow the instructions in the installer.

   .. image:: images/macOS/step5.png
      :alt: following installation instructions


#.
   Once installed you would see an Icon in top-right icon bar.

   .. image:: images/macOS/step6.png
      :alt: Docker icon



#. Click the whale icon in the icon bar to check “About Docker” page.


#. You can also open a terminal and run below command to get information on the installed docker.

.. code-block:: bash

   $ docker version


#. To check if Docker machine and Docker Compose are installed as well, use the command below.

.. code-block:: bash

   $ docker-compose version
   $ docker-machine version


#. You can also run a Dockerized web server to make sure everything works:

To run containers pull the container image:

.. code-block::

   docker pull registry.gitlab.com/librehealth/toolkit/lh-toolkit-docker:latest

You should see the similar console output:


.. image:: images/macOS/docker-pull.png
   :alt: pulling Docker


Navigate to the directory where you cloned this project. Depending on how you want to interact with the container, run it in foreground or as a daemon.
To run the container in the foreground:

.. code-block::

   docker-compose -f docker-compose.dev.yml up


.. image:: images/macOS/docker-up.png
   :alt: bringing container up


Now run this in your Browser at  localhost:8080/lh-toolkit


.. image:: images/macOS/docker-run.png
   :alt: accessing image

To run the container in the background:

.. code-block::

   docker-compose -f docker-compose.dev.yml up -d

Multiple Docker Versions
------------------------

Docker for Mac replaces docker and docker-compose with its own versions; if you already have Docker Toolbox on your Mac, Docker for Mac still replaces the binaries. You want the Docker client and Engine to match versions; mismatches can cause problems where the client and host cannot communicate. If you already have Docker Toolbox, and then you install Docker for Mac, you may get a newer version of the Docker client.
