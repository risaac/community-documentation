LibreHealth Toolkit Development Guide
-------------------------------------

.. toctree::
   :caption: LibreHealth Toolkit
   :maxdepth: 1

   docker-installation/index
   setting-up-ide/index
   installing-project/index
