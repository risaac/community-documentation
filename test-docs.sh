#!/usr/bin/env bash
set -eux
pip install -U pip ; \
pip install -U poetry ; \
poetry install ; \
make livehtml
